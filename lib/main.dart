import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:url_launcher/url_launcher.dart';

class CallService{
  void call(String number) => launch("tel:$number");
  void sendMsg(String number) => launch("sms:$number");
}

GetIt locator=GetIt();

void set(){
  locator.registerSingleton(CallService());
}

void main() {
  set();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sms And Call',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Message and Call API'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final CallService _service = locator<CallService>();
  TextEditingController _controller = TextEditingController();
  TextEditingController _controllerSms = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Form(
          child: ListView(
            children: <Widget>[
              TextFormField(
                controller: _controller,
                decoration: InputDecoration(
                  icon: Icon(Icons.phone)
                ),
              ),
              RaisedButton(
                  child: Text("Call"),
                  onPressed: ()=> _service.call(_controller.text)
              ),
              TextFormField(
                controller: _controllerSms,
                decoration: InputDecoration(
                    icon: Icon(Icons.message)
                ),
              ),
              RaisedButton(
                  child: Text("Message"),
                  onPressed: ()=> _service.sendMsg(_controllerSms.text)
              ),
            ],
          )
        ),
      ),
    );
  }
}
